import './react/css/main.global.css';
import React, { Fragment } from 'react';
import { render } from 'react-dom';
import { AppContainer as ReactHotAppContainer } from 'react-hot-loader';
import App from './react/App';

const AppContainer = process.env.PLAIN_HMR ? Fragment : ReactHotAppContainer;

document.addEventListener('DOMContentLoaded', () => {
    render(
        <AppContainer>
            <App />
        </AppContainer>,
        document.getElementById('root')
    );
});
