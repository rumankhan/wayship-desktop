import { Position, Toaster } from "@blueprintjs/core";

/** Singleton toaster instance. Create separate instances for different options. */
export const appToaster = Toaster.create({
	className: "ws-toast",
	position: Position.TOP_RIGHT,
});
