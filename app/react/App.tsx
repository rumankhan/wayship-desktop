import React from "react";
import { Provider } from "react-redux";
import store, { persistor } from "./redux/store";
import ScreenRoot from "./screens";
// tslint:disable-next-line: no-submodule-imports
import { PersistGate } from "redux-persist/integration/react";
import InfiniteProgress from "./components/_common/InfiniteProgress";

function App() {
    return (
        <Provider store={store}>
            <PersistGate
                loading={<InfiniteProgress isLoading={true} />}
                persistor={persistor}
            >
                <ScreenRoot />
            </PersistGate>
        </Provider>
    );
}

export default App;
