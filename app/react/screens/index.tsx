import React from 'react';
import {
    Route,
    BrowserRouter,
    Switch,
    Router,
    Redirect,
} from 'react-router-dom';
// import { rootRoutes, history } from "../routes";

// const routes = rootRoutes.children;
type Props = {};
function ScreenRoot(props: Props) {
    return (
        <div className="bg-gray-200 p-12 text-bold text-center font-redhat-display text-6xl text-gray-700 h-full flex flex-col justify-center leading-snug">
            <div>Hello Front End ;)</div>
            <p className="text-gray-600 text-lg font-redhat-text space-x-1">
                <span className="font-medium underline">React</span>,{' '}
                <span className="font-medium underline">Redux</span>,{' '}
                <span className="font-medium underline">Typescript</span> and{' '}
                <span className="font-medium underline">Blueprint</span>{' '}
                boilerplate
            </p>
        </div>
    );
}

/**
 * <BrowserRouter>
		<Root />
		<Switch>
			<Route
				exact={true}
				path="/"
				render={() => <Redirect to={routes.auth.children.login.url()} />}
			/>
		</Switch>
	</BrowserRouter>
 */

export default ScreenRoot;
