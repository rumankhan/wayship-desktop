import React from "react";
import classnames from 'classnames'
type CheckBoxType = React.PropsWithChildren<{
	className?:string;
}>;
const CheckBox: React.SFC<CheckBoxType> = ({ children, className }: CheckBoxType) => {
	return (
		<label className={classnames("flex justify-start items-start w-full mt-1", className)}>
			<div className="select-none flex-1 -mt-1">{children}</div>
			<div className="bg-transparent border-2 rounded border-green-600 w-4 h-4 flex flex-shrink-0 justify-center items-center ml-2 focus-within:border-blue-500 -mb-1">
				<input type="checkbox" className="opacity-0 absolute ws-checkbox" />
				<svg
					className="fill-current hidden w-2 h-2 text-green-500 pointer-events-none"
					viewBox="0 0 20 20"
				>
					<path d="M0 11l2-2 5 5L18 3l2 2L7 18z" />
				</svg>
			</div>
		</label>
	);
};

export default CheckBox;
