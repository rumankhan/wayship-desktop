import React from "react";
import classnames from "classnames";
type SVPlusProps = {
	className?: string;
};
const SVPlus: React.FC<SVPlusProps> = (props) => {
	return (
		<svg
			viewBox="0 0 28 28"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
			className={classnames("stroke-current", props.className)}
		>
			<path
				d="M14 7.14287V20.8572"
				strokeWidth="1"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M7.14282 14H20.8571"
				strokeWidth="1"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
};
export default SVPlus;
