import React from "react";
type SVMoreProps = {
	className?: string;
};
const SVMore: React.FC<SVMoreProps> = (props) => {
	return (
		<svg
			width="28"
			height="28"
			viewBox="0 0 28 28"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
			className={props.className}
		>
			<path
				d="M13 14.0001C13 14.5523 13.4477 15.0001 14 15.0001C14.5523 15.0001 15 14.5523 15 14.0001C15 13.4478 14.5523 13.0001 14 13.0001C13.4477 13.0001 13 13.4478 13 14.0001Z"
				stroke="#787885"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M13 21.0001C13 21.5523 13.4477 22.0001 14 22.0001C14.5523 22.0001 15 21.5523 15 21.0001C15 20.4478 14.5523 20.0001 14 20.0001C13.4477 20.0001 13 20.4478 13 21.0001Z"
				stroke="#787885"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M13 7.00006C13 7.55235 13.4477 8.00006 14 8.00006C14.5523 8.00006 15 7.55235 15 7.00006C15 6.44778 14.5523 6.00006 14 6.00006C13.4477 6.00006 13 6.44778 13 7.00006Z"
				stroke="#787885"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
};
export default SVMore;
