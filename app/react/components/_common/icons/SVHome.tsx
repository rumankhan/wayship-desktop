import React from 'react';
type SVHomeProps = {};
const SVHome: React.FC<SVHomeProps> = props => {
	return (
		<svg
			width="28"
			height="28"
			viewBox="0 0 28 28"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				d="M5.18359 11.0612L13.9999 4.20407L22.8163 11.0612V21.8367C22.8163 22.3563 22.6098 22.8547 22.2424 23.2221C21.875 23.5895 21.3767 23.7959 20.8571 23.7959H7.14278C6.62317 23.7959 6.12484 23.5895 5.75743 23.2221C5.39001 22.8547 5.18359 22.3563 5.18359 21.8367V11.0612Z"
				stroke="#19191D"
				strokeWidth="1.5"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M11.061 23.7959V14H16.9386V23.7959"
				stroke="#19191D"
				strokeWidth="1.5"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
};
export default SVHome;
