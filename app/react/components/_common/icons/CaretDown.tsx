import React from "react";
type IconCaretDownType = {};
const IconCaretDown: React.SFC<IconCaretDownType> = (
	props: IconCaretDownType
) => {
	return (
		<svg
			className="h-3 w-3"
			viewBox="0 0 19 11"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				d="M2.21667 7.62939e-05L9.5 6.83791L16.7833 7.62939e-05L19 2.08116L9.5 11.0001L0 2.08116L2.21667 7.62939e-05Z"
				fill="#142E57"
			/>
		</svg>
	);
};

export default IconCaretDown;
