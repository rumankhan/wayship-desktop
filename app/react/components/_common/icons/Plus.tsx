import React from "react";
type IconPlusType = {};
const IconPlus: React.SFC<IconPlusType> = (props: IconPlusType) => {
	return (
		<svg
			width="77"
			height="116"
			viewBox="0 0 77 116"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				d="M58 29V87"
				stroke="#787885"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M29 58H87"
				stroke="#787885"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
};

export default IconPlus;
