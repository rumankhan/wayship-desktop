import React from 'react';
type SVPersonProps = {};
const SVPerson: React.FC<SVPersonProps> = props => {
	return (
		<svg
			width="28"
			height="28"
			viewBox="0 0 28 28"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				d="M21.5521 22.5318V20.5725C21.5521 19.5332 21.1392 18.5365 20.4044 17.8017C19.6695 17.0668 18.6728 16.6539 17.6335 16.6539H9.79638C8.75711 16.6539 7.76041 17.0668 7.02553 17.8017C6.29066 18.5365 5.87781 19.5332 5.87781 20.5725V22.5318"
				stroke="#19191D"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M13.715 12.7354C15.8791 12.7354 17.6335 10.981 17.6335 8.81679C17.6335 6.65263 15.8791 4.89822 13.715 4.89822C11.5508 4.89822 9.79639 6.65263 9.79639 8.81679C9.79639 10.981 11.5508 12.7354 13.715 12.7354Z"
				stroke="#19191D"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
};
export default SVPerson;
