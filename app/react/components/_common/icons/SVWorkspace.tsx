import React from 'react';
type SVWorkspaceProps = {
	/*type rcprop here for prop type snippet*/
};
const SVWorkspace: React.FC<SVWorkspaceProps> = props => {
	return (
		<svg
			width="28"
			height="28"
			viewBox="0 0 28 28"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				d="M20.8571 5.18369H7.14278C6.06075 5.18369 5.18359 6.06084 5.18359 7.14287V20.8572C5.18359 21.9392 6.06075 22.8163 7.14278 22.8163H20.8571C21.9391 22.8163 22.8162 21.9392 22.8162 20.8572V7.14287C22.8162 6.06084 21.9391 5.18369 20.8571 5.18369Z"
				stroke="#133774"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M5.18359 11.0612H22.8162"
				stroke="#133774"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M11.061 22.8163V11.0612"
				stroke="#133774"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
};
export default SVWorkspace;
