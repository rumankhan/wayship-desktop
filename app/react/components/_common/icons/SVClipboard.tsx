import React from 'react';
type SVClipboardProps = {};
const SVClipboard: React.FC<SVClipboardProps> = props => {
	return (
		<svg
			width="28"
			height="28"
			viewBox="0 0 28 28"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				d="M17.9182 6.16307H19.8774C20.397 6.16307 20.8953 6.36948 21.2627 6.7369C21.6301 7.10432 21.8366 7.60265 21.8366 8.12225V21.8365C21.8366 22.3561 21.6301 22.8545 21.2627 23.2219C20.8953 23.5893 20.397 23.7957 19.8774 23.7957H8.12227C7.60266 23.7957 7.10434 23.5893 6.73692 23.2219C6.3695 22.8545 6.16309 22.3561 6.16309 21.8365V8.12225C6.16309 7.60265 6.3695 7.10432 6.73692 6.7369C7.10434 6.36948 7.60266 6.16307 8.12227 6.16307H10.0815"
				stroke="#19191D"
				strokeWidth="1.5"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M16.9387 4.20389H11.0611C10.5201 4.20389 10.0815 4.64247 10.0815 5.18348V7.14266C10.0815 7.68368 10.5201 8.12226 11.0611 8.12226H16.9387C17.4797 8.12226 17.9183 7.68368 17.9183 7.14266V5.18348C17.9183 4.64247 17.4797 4.20389 16.9387 4.20389Z"
				stroke="#19191D"
				strokeWidth="1.5"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
};
export default SVClipboard;
