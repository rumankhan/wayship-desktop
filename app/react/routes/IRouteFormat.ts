interface IRouteFormat {
	path: string;
	url: (...params: any) => string;
	isOnSidebar?: boolean;
	authRequired?: boolean;
	children: {
		[key: string]: IRouteFormat;
	};
}
export default IRouteFormat;
