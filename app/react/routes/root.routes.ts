import IRouteFormat from "./IRouteFormat";

const rootRoutes = {
	path: "/",
	url: () => "/",
	children: {},
};
export default rootRoutes;
