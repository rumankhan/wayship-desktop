import { createStore, Store } from "redux";
import rootReducer from "./reducers";
import enhancer from "./enhancers";
import { persistStore } from "redux-persist";
const store: Store<IStore> = createStore(rootReducer, enhancer);
export default store;
export const persistor = persistStore(store);
export type IStore = ReturnType<typeof rootReducer>;

