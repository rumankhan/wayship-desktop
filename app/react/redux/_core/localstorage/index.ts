import localStorageMiddleware from "./middlewares";
export { default as localStorageAx } from "./actions";
export { default as localStorageActionTypes } from "./action-types";
export default localStorageMiddleware;
