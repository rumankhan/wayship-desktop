const localstorageActionTypes = {
	LOCALSTORAGE_SET_ITEM: "localstorage/SET-ITEM",
	LOCALSTORAGE_REMOVE_ITEM: "localstorage/REMOVE-ITEM",
	LOCALSTORAGE_CLEAR: "localstorage/CLEAR",
};
export default localstorageActionTypes;
