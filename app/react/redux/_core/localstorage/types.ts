import { IAction } from "../../_common/type";

export type ILocalstorageSetItem__Action = IAction<{
	key: string;
	value: any;
}>;

export type ILocalstorageRemoveItem__Action = IAction<{ key: string }>;
