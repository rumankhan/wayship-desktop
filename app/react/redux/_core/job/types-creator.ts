import constants from "./action-types";
const { JOB_ACTIVE, JOB_ERROR, JOB_IDLE, JOB_SUCCESS } = constants;
const jobTypesCreator = (jobName: string) => ({
	JOB_ACTIVE: `${jobName} [${JOB_ACTIVE}]`,
	JOB_ERROR: `${jobName} [${JOB_ERROR}]`,
	JOB_IDLE: `${jobName} [${JOB_IDLE}]`,
	JOB_SUCCESS: `${jobName} [${JOB_SUCCESS}]`,
});
export default jobTypesCreator;
