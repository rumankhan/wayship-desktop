import {
	IToastProps,
	IconName,
	IActionProps,
	ILinkProps,
} from "@blueprintjs/core";
import { IAction } from "../../_common/type";

export type IJobState = "IDLE" | "ACTIVE" | "SUCCESS" | "ERROR";
export type IJobNotification = {
	/** Message to display in the body of the toast. takes jobs `message` as default */
	message?: string;
	/** Visual intent color to apply to element. takes jobs `state` as default */
	intent?: IJobState;
	/** A space-delimited list of class names to pass along to a child element. */
	className?: string;
	/**
	 * This shows the toast at a given state. `By default it choosess the same state at which notification was created`
	 */
	showAtState?: IJobState;
	/**
	 * This hides the toast at a given state.
	 * @default "IDLE"
	 */
	hideAtState?: IJobState;
	/** Name of a Blueprint UI icon (or an icon element) to render before the message. */
	icon?: IconName;
	/**
	 * Action rendered as a minimal `AnchorButton`. The toast is dismissed automatically when the
	 * user clicks the action button. Note that the `intent` prop is ignored (the action button
	 * cannot have its own intent color that might conflict with the toast's intent). Omit this
	 * prop to omit the action button.
	 */
	action?: IActionProps & ILinkProps;
	/**
	 * Milliseconds to wait before automatically dismissing toast.
	 * Providing a value less than or equal to 0 will disable the timeout (this is discouraged).
	 * Note: The timeout starts at `hideAtState` prop state. it means that once jobState === hideAtState the timeout gets started
	 * @default 5000
	 */
	timeout?: number;
};

export interface IJobStore {
	/**
	 * id to reference a unique job
	 * (auto generated)
	 */
	id: string;
	state: IJobState;
	message?: string;
	notification?: IJobNotification;
}

export interface IJobReducerState {
	[key: string]: IJobStore;
}
export interface IJobHOCState {
	_jobs: IJobReducerState;
}

export type IJob__Action = IAction<{
	message?: string;
	notification?: IJobNotification;
	[key: string]: any;
}>;

export type IJob__Action_idle = IAction<
	{
		message?: string;
		notification?: IJobNotification;
		JOB_NAME: string;
		[key: string]: any;
	},
	{ hasGlobalEffect: boolean }
>;
export type IJob__Action_active = IAction<
	{
		message?: string;
		notification?: IJobNotification;
		JOB_NAME: string;
		[key: string]: any;
	},
	{ hasGlobalEffect: boolean }
>;
export type IJob__Action_error = IAction<
	{
		message?: string;
		notification?: IJobNotification;
		JOB_NAME: string;
		[key: string]: any;
	},
	{ hasGlobalEffect: boolean }
>;
export type IJob__Action_success = IAction<
	{
		message?: string;
		notification?: IJobNotification;
		JOB_NAME: string;
		[key: string]: any;
	},
	{ hasGlobalEffect: boolean }
>;

export interface IJobGetStatus {
	isLoading: boolean;
	isError: boolean;
	isSuccess: boolean;
	isIdle: boolean;
	message?: string;
	/**
	 * Is Job executed atleast once?
	 */
	isExecuted: boolean;
}
