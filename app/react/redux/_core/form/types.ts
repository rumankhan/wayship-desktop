import { IAction } from "../../_common/type";
type KeysTypeAssigner<ISource, IDestinationType> = {
	[P in keyof Required<ISource>]: IDestinationType;
};
export type IFormFieldsFormat = {
	[key: string]: any;
};
export type IFormValidityFormat = {
	isValid: boolean;
	message: string;
};
export type IFormErrorFormat = {
	GLOBAL: {
		isValid: boolean;
		message: string;
	};
	[key: string]: IFormValidityFormat;
};

export type IFormErrorFormatTemp<T> = KeysTypeAssigner<
	T,
	IFormValidityFormat
> & {
	GLOBAL: {
		isValid: boolean;
		message: string;
	};
};

export type IFormHelpMessagesTemp<T> = KeysTypeAssigner<T, string>;
export type IFormHelpMessagesFormat = {
	[key: string]: string;
};

export interface IForm<
	IFormFields extends IFormFieldsFormat,
	IFieldErrors extends IFormErrorFormat = IFormErrorFormatTemp<IFormFields>,
	IFormHelpMessages extends IFormHelpMessagesFormat = IFormHelpMessagesTemp<
		IFormFields
	>,
	IFormWarnings extends IFormErrorFormat = IFormErrorFormatTemp<IFormFields>
> {
	/**
	 * All Fields of Form
	 */
	fields: IFormFields;
	/**
	 * All Errors for form
	 */
	errors: IFieldErrors;
	/**
	 * All Warnings for form
	 */
	warnings: IFormWarnings;
	/**
	 * Any helpMessages you want to show to user
	 */
	helpMessages: IFormHelpMessages;
	/**
	 * Is Form is used to create/edit a object
	 */
	mode: "CREATE" | "EDIT" | "NONE";
}

export type IForm_FieldsSet__Action<
	IFormFields extends IFormFieldsFormat
> = IAction<{
	formName: string;
	feature: string;
	fields: Partial<IFormFields>;
}>;
export type IForm_FieldsClear__Action = IAction<{
	formName: string;
	feature: string;
	fieldKeys: string[];
}>;
export type IForm_ErrorsSet__Action<
	IFieldErrors extends IFormErrorFormat
> = IAction<{
	formName: string;
	feature: string;
	errors: Partial<IFieldErrors>;
}>;
export type IForm_ErrorsReset__Action = IAction<{
	formName: string;
	feature: string;
	fieldKeys: string[];
}>;
export type IForm_WarningsSet__Action<
	IFormWarnings extends IFormErrorFormat = any
> = IAction<{
	formName: string;
	feature: string;
	warnings: IFormWarnings;
}>;
export type IForm_WarningsReset__Action = IAction<{
	formName: string;
	feature: string;
	fieldKeys: string[];
}>;
export type IForm_HelpmessagesSet__Action<
	IFormHelpMessages extends IFormHelpMessagesFormat = any
> = IAction<{
	formName: string;
	feature: string;
	helpMessages: IFormHelpMessages;
}>;
export type IForm_HelpmessagesReset__Action = IAction<{
	formName: string;
	feature: string;
	fieldKeys: string[];
}>;
export type IForm_Reset__Action = IAction<{
	formName: string;
	feature: string;
	fieldKeys: string[];
}>;
export type IForm_Submit__Action = IAction<{
	formName: string;
	feature: string;
}>;
export type IForm_modeSet__Action = IAction<{
	formName: string;
	feature: string;
	mode: "CREATE" | "EDIT" | "NONE";
}>;

export type IForm__Get<T> = {
	hasErrors: boolean;
	hasWarnings: boolean;
} & IForm<T>;

export interface IFormStore {
	_forms: {
		[formName: string]: IForm<any>;
	};
}
