import { IAction } from "../../_common/type";
import { IJobNotification } from "../job/types";

export type INotification__Action_set = IAction<{
	JOB_NAME: string;
	notification: IJobNotification;
}>;

export type INotificationStore = {
	[JOB_NAME: string]: IJobNotification;
};
