const notificationActionTypes = {
	NOTIFICATION_SET: "core/notification/SET",
};
export default notificationActionTypes;
