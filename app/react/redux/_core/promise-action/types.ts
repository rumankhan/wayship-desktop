import { Action } from "redux";

export type IPromiseAction<ISuccess, IError> = {
	preExecute?: () => void;
	onSuccess?: (successResult: ISuccess) => void;
	onError?: (error: IError) => void;
	finally?: () => void;
	invoker: string;
};

export type IPromiseReturn = {
	id: string;
	payload: Promise<any>;
	meta: {
		preExecute: () => void;
		onSuccess: (successResult: any) => void;
		onError: (error: any) => void;
		finally: () => void;
	};
};
