const PROMISE_ACTION = "promise/PROCESS";
const promiseActionTypes = {
	PROMISE_ACTION,
};
export default promiseActionTypes;
