const MULTI_DISPATCH_ACTION = "multiAction/DISPATCH_EACH";
const MULTI_BATCH_ACTION = "multiAction/BATCH_ALL";

const multiActionActionTypes = {
	MULTI_DISPATCH_ACTION,
	MULTI_BATCH_ACTION,
};

export default multiActionActionTypes;
