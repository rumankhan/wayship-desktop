export interface IHistoryStore {
	canUpdateRoute: boolean;
	url: string;
	method: "PUSH" | "POP" | "REPLACE" | "CLEAR" | "RESET";
}
