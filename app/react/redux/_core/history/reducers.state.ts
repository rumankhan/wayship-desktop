import { IHistoryStore } from "./types";

const reducerState: IHistoryStore = {
	canUpdateRoute: false,
	method: "CLEAR",
	url: "",
};

export default reducerState;
