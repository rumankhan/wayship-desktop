import constants from "./constants";
const { HISTORY_CLEAR, HISTORY_POP, HISTORY_PUSH, HISTORY_REPLACE } = constants;
const historyTypesCreator = (feature: string = "@") => ({
	HISTORY_PUSH: `${feature} [${HISTORY_PUSH}]`,
	HISTORY_POP: `${feature} [${HISTORY_POP}]`,
	HISTORY_REPLACE: `${feature} [${HISTORY_REPLACE}]`,
	HISTORY_CLEAR: `${feature} [${HISTORY_CLEAR}]`,
});
export default historyTypesCreator;
