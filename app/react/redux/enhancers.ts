import { multiActionMiddleware } from "./_core/multi-action";
import trueTypeMiddleware from "./_core/true-type";
import { historyMiddleware } from "./_core/history";
import { apiMiddleware } from "./_core/api";
import { applyMiddleware, compose, Middleware } from "redux";
import { promiseMiddleware } from "./_core/promise-action";
import { notificationMiddleware } from "./_core/notification";

const composeEnhancers =
	typeof window === "object" &&
	(window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
		? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
				// Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
		  })
		: compose;

const coreMiddlewares = [
	multiActionMiddleware,
	promiseMiddleware,
	trueTypeMiddleware,
	historyMiddleware,
	apiMiddleware,
	notificationMiddleware,
];
const featureMiddlewares: Middleware[] = [];
const enhancer = composeEnhancers(
	applyMiddleware(
		...coreMiddlewares,
		...featureMiddlewares
		// Add middlewares here
	)
	// other store enhancers if any
);

export default enhancer;
