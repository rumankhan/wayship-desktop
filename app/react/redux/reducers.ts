import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
// tslint:disable-next-line: no-submodule-imports
import storage from "redux-persist/lib/storage";
import historyReducers from "./_core/history";
import notificationReducers from "./_core/notification";
import jobsReducers, { jobGlobalReducer } from "./_core/job";

const appPersistConfig = {
	key: "app",
	storage,
	whitelist: [],
};
const rootReducer = combineReducers({
	core: combineReducers({
		history: historyReducers,
		notification: notificationReducers,
		jobGlobal: jobGlobalReducer,
	}),
	app: persistReducer(appPersistConfig, combineReducers({})),
});
export default rootReducer;
