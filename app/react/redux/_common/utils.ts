import { IListObjectStore, IAction } from "./type";
import { Action } from "redux";

const arrayToObject = <T extends { id: string }>(list: T[]) => {
	let objectFormat: IListObjectStore<T> = {
		byIds: {},
		ids: [],
	};
	list.map(
		(item) =>
			(objectFormat = {
				...objectFormat,
				byIds: {
					...objectFormat.byIds,
					[item.id]: item,
				},
				ids: [...objectFormat.ids, item.id],
			})
	);
	return objectFormat;
};
const objectToArray = <T extends { id: string }>(object: IListObjectStore<T>) =>
	object.ids.map((id) => object.byIds[id]);

function getFeatureAction(feature: string, action: IAction<any>): IAction<any> {
	return {
		type: `${feature} [${action.type}]`,
		payload: action.payload,
		meta: {
			...action.meta,
			trueType: action.type,
			feature,
		},
	};
}

function capitalize(stringText: any) {
	if (!stringText) return stringText;
	stringText = stringText.split(" ");
	for (let i = 0, x = stringText.length; i < x; i++) {
		stringText[i] =
			stringText[i][0].toUpperCase() +
			(stringText[i] as string).toLowerCase().substr(1);
	}

	return stringText.join(" ");
}

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce<Params extends any[]>(
	func: (...args: Params) => any,
	timeout: number
): (...args: Params) => void {
	let timer: NodeJS.Timeout;
	return (...args: Params) => {
		clearTimeout(timer);
		timer = setTimeout(() => {
			func(...args);
		}, timeout);
	};
}

const commonUtils = {
	arrayToObject,
	objectToArray,
	getFeatureAction,
	capitalize,
	debounce,
};

export default commonUtils;
