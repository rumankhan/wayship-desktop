import { Action } from "redux";

export interface IListObjectStore<T extends { id: string }> {
	byIds: {
		[key: string]: T;
	};
	ids: string[];
}

export type IAction<T, Meta = any> = Action<string> & {
	payload: T;
	meta?: Meta & {
		[key: string]: any;
	};
};
